import cv2
import numpy as np
import imutils

cap = cv2.VideoCapture("35.mp4")    # mp4 file reading


def canny_edge_detector(image):     # finding edges and smoothing them
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray_image, (5, 5), 0)
    canny = cv2.Canny(blur, 50, 150)
    return canny


def region_of_interest(image):
    height = image.shape[0]
    polygons = np.array([
        [(350, height-100), (1100, height-100), (720, 370),(580, 370)]  # essential: determination of a region for input-dependent line finding
    ])
    mask = np.zeros_like(image)

    cv2.fillPoly(mask, polygons, 255)   # applying a mask with a black background to a region we are not interested in
    masked_image = cv2.bitwise_and(image, mask)
    return masked_image


def create_coordinates(image, line_parameters):     # detection of the coordinates of the lane
    try:
        slope, intercept = line_parameters
    except TypeError:
        slope, intercept = 1, 1

    y1 = (image.shape[0])
    y2 = int(y1 * (3 / 5))
    x1 = int((y1 - intercept) / slope)
    x2 = int((y2 - intercept) / slope)
    return np.array([x1, y1, x2, y2])


def average_slope_intercept(image, lines):  # detecting which lane is right and which is left after positive or negative
                                            # gradients, if negative then left lane, if positive then right lane
    left_fit = []
    right_fit = []
    for line in lines:
        x1, y1, x2, y2 = line.reshape(4)

        parameters = np.polyfit((x1, x2), (y1, y2), 1)
        slope = parameters[0]
        intercept = parameters[1]
        if slope < 0:
            left_fit.append((slope, intercept))
        else:
            right_fit.append((slope, intercept))

    left_fit_average = np.average(left_fit, axis=0)
    right_fit_average = np.average(right_fit, axis=0)
    left_line = create_coordinates(image, left_fit_average)
    right_line = create_coordinates(image, right_fit_average)
    return np.array([left_line, right_line])


def display_lines(image, lines):    # drawing lines
    line_image = np.zeros_like(image)
    if lines is not None:
        for x1, y1, x2, y2 in lines:
            if x1 < 0:
                x1 = 0
            elif x1 > image.shape[1]:
                x1 = image.shape[1]

            if x2 < 0:
                x2 = 0
            elif x2 > image.shape[1]:
                x2 = image.shape[1]

            if y1 < 0:
                y1 = 0
            elif y1 > image.shape[0]:
                y1 = image.shape[0]

            if y2 < 0:
                y2 = 0
            elif y2 > image.shape[0]:
                y2 = image.shape[0]

            cv2.line(line_image, (int(x1), int(y1)), (int(x2), int(y2)), (0, 0, 255), 10)
    return line_image


if __name__ == '__main__':
    while (cap.isOpened()):
        ret, frame = cap.read()     # rozbicie plku mp4 na poszczegulne klatki(zdjęcia)
        frame = imutils.resize(frame, width=1200)
        if ret is True:
            canny_image = canny_edge_detector(frame)
            cropped_image = region_of_interest(canny_image)

            lines = cv2.HoughLinesP(cropped_image, 2, np.pi / 180, 100,     # znajdowanie segmentów lini w obrazie
                                    np.array([]), minLineLength=50,
                                    maxLineGap=10)

            try:
                averaged_lines = average_slope_intercept(frame, lines)
                line_image = display_lines(frame, averaged_lines)
                combo_image = cv2.addWeighted(frame, 0.8, line_image, 1, 1)     # nałożenie na obraz wykrytych lini
            except TypeError:
                combo_image = frame

            cv2.imshow("results", cropped_image)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break

    cap.release()
    cv2.destroyAllWindows()



