from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import img_to_array
import numpy as np
import cv2
import pickle
from imutils import resize
import road_line_detection


signs_names = ["Speed limit (20km/h)",
               "Speed limit (30km/h)",
               "Speed limit (50km/h)",
               "Speed limit (60km/h)",
               "Speed limit (70km/h)",
               "Speed limit (80km/h)",
               "End of speed limit (80km/h)",
               "Speed limit (100km/h)",
               "Speed limit (120km/h)",
               "No passing",
               "No passing for vehicles over 3.5 metric tons",
               "Right-of-way at the next intersection",
               "Priority road",
               "Yield",
               "Stop",
               "No vehicles",
               "Vehicles over 3.5 metric tons prohibited",
               "No entry",
               "General caution",
               "Dangerous curve to the left",
               "Dangerous curve to the right",
               "Double curve",
               "Bumpy road",
               "Slippery road",
               "Road narrows on the right",
               "Road work",
               "Traffic signals",
               "Pedestrians",
               "Children crossing",
               "Bicycles crossing",
               "Beware of ice/snow",
               "Wild animals crossing",
               "End of all speed and passing limits",
               "Turn right ahead",
               "Turn left ahead",
               "Ahead only",
               "Go straight or right",
               "Go straight or left",
               "Keep right",
               "Keep left",
               "Roundabout mandatory",
               "End of no passing",
               "End of no passing by vehicles over 3.5 metric tons"]


BLOB_INPUT_DIMENSIONS = (320, 320)
FRAME_WIDTH = 1200

model = load_model('model/signs_classifier.h5')
labels = pickle.loads(open('model/labels.pickle', 'rb').read())

cap = cv2.VideoCapture("Data/videos/35.mp4")
# cap = cv2.VideoCapture("Data/videos/2021_0728_065929_269.MOV")
# cap = cv2.VideoCapture("Data/videos/traffic-sign-to-test.mp4")

# Extracting class names from file
class_file = "Data/traffic_signs_yolo_format/signs_names.names"
with open(class_file, "rt") as f:
    class_names = f.read().rstrip("\n").split("\n")

# Paths to yolov4-tiny weights and configuration files
model_configuration_path = "weights/yolov4-tiny_training.cfg"
model_weights_path = "weights/yolov4-tiny_training_last.weights"

# Creating yolov4-tiny model
net = cv2.dnn.readNetFromDarknet(model_configuration_path, model_weights_path)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)


def find_objects(outputs, img):
    conf_threshold = 0.5
    nms_threshold = 0.3
    height_target, width_target, channels_target = img.shape
    bbox = []
    class_ids = []
    confs = []

    # Looping through each output
    for output in outputs:

        # Looping through each bounding box in an output
        for detection in output:
            # Probability of each from 4 classes
            scores = detection[5:]

            # Index of class with highest probability
            class_id = np.argmax(scores)

            # Probability value of the most probable class
            confidence = scores[class_id]

            # Displaying bounding boxes with confidence > 50%
            if confidence > conf_threshold:
                width, height = int(detection[2] * width_target), int(detection[3] * height_target)
                x_top_left, y_top_left = int(detection[0] * width_target - width/2), int(detection[1] * height_target - height/2)
                bbox.append((x_top_left, y_top_left, width, height))
                class_ids.append(class_id)
                confs.append(float(confidence))

    # Non-maximum suppression to prevent overlapping of bounding boxes
    indices = cv2.dnn.NMSBoxes(bbox, confs, conf_threshold, nms_threshold)

    # Looping through indices of remaining bounding boxes
    for i in indices:
        i = i[0]
        box = bbox[i]

        # label_print = class_names[class_ids[i]].upper()
        # label_conf_percentage = int(confs[i] * 100)

        x, y, width, height = box[0], box[1], box[2], box[3]

        if x < 0:
            x = 0
        elif x > img.shape[1]:
            x = img.shape[1]

        if y < 0:
            y = 0
        elif y > img.shape[0]:
            y = img.shape[0]

        roi = cv2.resize(img[y: y + height, x: x + width], dsize=(64, 64))
        roi = img_to_array(roi) / 255.0
        roi = np.expand_dims(roi, axis=0)

        preds = model.predict(roi)[0]
        pred = np.argmax(preds)
        pred_value = np.max(preds) * 100
        class_label = signs_names[pred]

        # Drawing bounding boxes and putting labels
        cv2.rectangle(img,
                      pt1=(x, y),
                      pt2=(x + width, y + height),
                      color=(255, 0, 0),
                      thickness=2)

        cv2.putText(img,
                    text=f"{class_label}: {int(pred_value)}%",
                    org=(x, y - 10),
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=0.6,
                    color=(255, 0, 0),
                    thickness=2)


while True:
    ret, frame = cap.read()

    if ret is True:
        # Resizing frame to fixed width, keeping original aspect ratio
        frame = resize(frame, width=FRAME_WIDTH)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        canny_image = road_line_detection.canny_edge_detector(frame)
        cropped_image = road_line_detection.region_of_interest(canny_image)

        lines = cv2.HoughLinesP(cropped_image, 2, np.pi / 180, 100,
                                np.array([]), minLineLength=50,
                                maxLineGap=10)

        # Converting frame format into blob
        blob = cv2.dnn.blobFromImage(frame,
                                     scalefactor=1./255,
                                     size=BLOB_INPUT_DIMENSIONS,
                                     mean=[0, 0, 0],
                                     swapRB=1,
                                     crop=False)

        # Setting blob frame as an input for yolo
        net.setInput(blob)

        # Extracting names of output layers
        layer_names = net.getLayerNames()
        output_names = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

        # Extracting predictions from output layers
        outputs = net.forward(output_names)

        find_objects(outputs, frame)

        try:
            averaged_lines = road_line_detection.average_slope_intercept(frame, lines)
            line_image = road_line_detection.display_lines(frame, averaged_lines)
            combo_image = cv2.addWeighted(frame, 0.8, line_image, 1, 1)
        except TypeError:
            combo_image = frame

        combo_image = cv2.cvtColor(combo_image, cv2.COLOR_RGB2BGR)

        cv2.imshow("Image", combo_image)
        if cv2.waitKey(1) == ord("q"):
            break
    else:
        break
