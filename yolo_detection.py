import cv2
import numpy as np
from imutils import resize


BLOB_INPUT_DIMENSIONS = (320, 320)
FRAME_WIDTH = 1200

cap = cv2.VideoCapture("Data/videos/35.mp4")

# Extracting class names from file
class_file = "Data/traffic_signs_yolo_format/signs_names.names"
with open(class_file, "rt") as f:
    class_names = f.read().rstrip("\n").split("\n")

# Paths to yolov4-tiny weights and configuration files
model_configuration_path = "weights/yolov4-tiny_training.cfg"
model_weights_path = "weights/yolov4-tiny_training_last.weights"

# Creating yolov4-tiny model
net = cv2.dnn.readNetFromDarknet(model_configuration_path, model_weights_path)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)


def find_objects(outputs, img):
    conf_threshold = 0.5
    nms_threshold = 0.3
    height_target, width_target, channels_target = img.shape
    bbox = []
    class_ids = []
    confs = []

    # Looping through each output
    for output in outputs:

        # Looping through each bounding box in an output
        for detection in output:
            # Probability of each from 4 classes
            scores = detection[5:]

            # Index of class with highest probability
            class_id = np.argmax(scores)

            # Probability value of the most probable class
            confidence = scores[class_id]

            # Displaying bounding boxes with confidence > 50%
            if confidence > conf_threshold:
                width, height = int(detection[2] * width_target), int(detection[3] * height_target)
                x_top_left, y_top_left = int(detection[0] * width_target - width/2), int(detection[1] * height_target - height/2)
                bbox.append((x_top_left, y_top_left, width, height))
                class_ids.append(class_id)
                confs.append(float(confidence))

    # Non-maximum suppression to prevent overlapping of bounding boxes
    indices = cv2.dnn.NMSBoxes(bbox, confs, conf_threshold, nms_threshold)

    # Looping through indices of remaining bounding boxes
    for i in indices:
        i = i[0]
        box = bbox[i]

        label_print = class_names[class_ids[i]].upper()
        label_conf_percentage = int(confs[i] * 100)

        x, y, width, height = box[0], box[1], box[2], box[3]

        # Drawing bounding boxes and putting labels
        cv2.rectangle(img,
                      pt1=(x, y),
                      pt2=(x + width, y + height),
                      color=(0, 255, 0),
                      thickness=2)

        cv2.putText(img,
                    text=f"{label_print}: {label_conf_percentage}%",
                    org=(x, y - 10),
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=0.6,
                    color=(0, 255, 0),
                    thickness=2)


while True:
    ret, frame = cap.read()

    if ret is True:
        # Resizing frame to fixed width, keeping original aspect ratio
        frame = resize(frame, width=FRAME_WIDTH)

        # Converting frame format into blob
        blob = cv2.dnn.blobFromImage(frame,
                                     scalefactor=1./255,
                                     size=BLOB_INPUT_DIMENSIONS,
                                     mean=[0, 0, 0],
                                     swapRB=1,
                                     crop=False)

        # Setting blob frame as an input for yolo
        net.setInput(blob)

        # Extracting names of output layers
        layer_names = net.getLayerNames()
        output_names = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

        # Extracting predictions from output layers
        outputs = net.forward(output_names)

        find_objects(outputs, frame)

        cv2.imshow("Image", frame)
        if cv2.waitKey(1) == ord("q"):
            break
    else:
        break
