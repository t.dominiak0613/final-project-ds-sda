import cv2
from tensorflow.keras.layers import Dense, Dropout, Conv2D, Flatten, MaxPooling2D
from tensorflow.keras.models import Sequential
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.optimizers import Adam
import pickle
import matplotlib.pyplot as plt
import numpy as np


BATCH_SIZE = 128
TARGET_SIZE = (64, 64)
EPOCHS = 20
IMAGES_PATH = "Data/GTSRB/Final_Training/Images"
INPUT_SHAPE = (64, 64, 3)

early_stopping = EarlyStopping(monitor="val_accuracy", patience=3, restore_best_weights=True)

train_datagen = ImageDataGenerator(rescale=1./255,
                                   width_shift_range=0.15,
                                   height_shift_range=0.15,
                                   rotation_range=15,
                                   zoom_range=0.05,
                                   shear_range=0.15,
                                   brightness_range=(0.8, 1.2),
                                   fill_mode="nearest",
                                   validation_split=0.2,
                                   )

validation_datagen = ImageDataGenerator(rescale=1./255,
                                        validation_split=0.2,)

training_generator = train_datagen.flow_from_directory(IMAGES_PATH,
                                                       target_size=TARGET_SIZE,
                                                       batch_size=BATCH_SIZE,
                                                       class_mode="categorical",
                                                       subset="training",
                                                       seed=42,
                                                       shuffle=True,
                                                       color_mode="rgb",
                                                       )

validation_generator = validation_datagen.flow_from_directory(IMAGES_PATH,
                                                              target_size=TARGET_SIZE,
                                                              batch_size=BATCH_SIZE,
                                                              class_mode="categorical",
                                                              subset="validation",
                                                              seed=42,
                                                              shuffle=True,
                                                              color_mode="rgb")


optimizer = Adam(learning_rate=0.001)

model = Sequential()
model.add(Conv2D(filters=32, kernel_size=(5, 5), input_shape=INPUT_SHAPE, activation="relu"))
model.add(MaxPooling2D((2, 2)))
model.add(Conv2D(filters=64, kernel_size=(3, 3), input_shape=INPUT_SHAPE, activation="relu"))
model.add(MaxPooling2D((2, 2)))
model.add(Conv2D(filters=64, kernel_size=(3, 3), input_shape=INPUT_SHAPE, activation="relu"))
model.add(Flatten())
model.add(Dense(512, activation="relu"))
model.add(Dropout(0.3))
model.add(Dense(256, activation="relu"))
model.add(Dense(128, activation="relu"))
model.add(Dense(64, activation="relu"))
model.add(Dense(43, activation="softmax"))

model.compile(loss="categorical_crossentropy",
              optimizer=optimizer,
              metrics=["accuracy"])

model.summary()

H = model.fit(x=training_generator, validation_data=validation_generator, epochs=EPOCHS, callbacks=[early_stopping])

model.save('model/signs_classifier.h5')
labels = training_generator.class_indices
f = open("model/labels.pickle", "wb")
f.write(pickle.dumps(labels))
f.close()

N = EPOCHS
plt.figure()
plt.plot(np.arange(0, N), H.history["accuracy"], label="train_accuracy")
plt.plot(np.arange(0, N), H.history["val_accuracy"], label="val_accuracy")

plt.title("Training Loss and Accuracy")
plt.xlabel("Epochs")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.savefig("model/plot.png")
